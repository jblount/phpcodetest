<?php 
	$response = array();
	
	function arrMean($arr){
		//mean
		return round(array_sum($arr) / count($arr), 3);
	}
	
	function arrMode($arr){
		//mode
		$values = array_count_values($arr);
		return array_search(max($values), $values);
	}
	
	function arrMedian($arr){
		//median
		rsort($arr);
		return $arr[ round(count($arr) / 2) - 1 ];
	}
	
	function arrRange($arr){
		//range 
		sort($arr);
		$large = end($arr);
		$small = $arr[0]; 
		return $large - $small;
	}
	
	try{
		if($_GET){
			$response['error']['code'] = 404;
			throw new Exception('GET requests are not accepted');
		}
		//get json data
		$data = array();
		$data = json_decode(file_get_contents("php://input"), true);
		if(!isset($data['numbers']) || empty($data['numbers'])) 
			throw new Exception('No data recieved or incorrect format');
		
		//process data
		$response['results'] = array();
		foreach($data['numbers'] as &$v){
			if(gettype($v) != 'integer') throw new Exception('Non-integer received');
		}

		$response['results']['mean'] = arrMean($data['numbers']);
		$response['results']['median'] = arrMedian($data['numbers']);
		$response['results']['mode'] = arrMode($data['numbers']);
		$response['results']['range'] = arrRange($data['numbers']);
		
	}catch(Exception $e){
		if(isset($response['results'])) unset($response['results']);
		if(!isset($response['error']['code']) || !$response['error']['code']){
			$response['error']['code'] = 500; // default error code
		}
		$response['error']['message'] = $e->getMessage();
	}
	
	echo json_encode($response);
?>