PHP Code Test for BidStart / Stanley Gibbons

Jason Blount

jasondblount@gmail.com

01/28/2014



Description:

This is a simple API that will calculate mean, median, mode and range based on an input JSON POST, returned in JSON format.


Usage:

Use either the included index.php form or POST your JSON query as an array of numbers named "numbers" to "/mmmr". The script will output a JSON formatted string with the results in an associative array named "results", or will output an "error" array with "code" and "message" values.


Files:

index.php - Contains a form for testing the API via an AJAX call

mmmr.php - The script that calculates and outputs the mean, median, mode and range

.htaccess - Contains rewrite rules to route "/mmmr" to mmmr.php

README.md - This file